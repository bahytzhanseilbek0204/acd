package Midka1;

public class LinkedList {
    private Node head;
    private Node tail;

    public LinkedList() {
        head = null;
        tail = null;
    }

    public void insert(int data) {
        Node newNode = new Node(data);
        if (head == null) {
            head = newNode;
        } else {
            Node temp = head;
            head = newNode;
            newNode.next = temp;
        }
    }
    public void show(){
        Node current = head;
        while (current.next != null) {
            System.out.println(current.data);
            current= current.next;
        }
        System.out.println(current.data);
    }


    public void remove(int counter) {
        if (head == null)
            return;
        Node node = head;
        if (counter == 0) {
            head = node.next;
            return;
        }
        if (node != null) {
            for (int i = 0; i < counter - 1; i++)
                node = node.next;

            if (node == null || node.next == null)
                return;
            Node next = node.next.next;
            node.next = next;
        }
    }



}


