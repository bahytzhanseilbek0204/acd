package Midka1;

public class Task1 {
/* Test
1)B
2)D
3)B
4)D
5)E
 */

    public static void main(String[] args) {
        System.out.println("aabbcss " + stringClean("aabbcss"));
        System.out.println("abbbcdd " + stringClean("abbbcdd"));
        System.out.println("Hello " + stringClean("Hello"));

    }

    public static String stringClean(String str) {
        for (int i = 1; i < (str.length()); i++) {
            if (str.charAt(i - 1) == str.charAt(i))
                return stringClean(str.substring(0, i - 1) + str.substring(i, str.length()));
        }
        return str;
    }
}

