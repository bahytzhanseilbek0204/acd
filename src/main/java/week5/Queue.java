package week5;

public class Queue {
    private Node head;
    private Node tail;
    private int size;

    public Queue(){
        head = null;
        tail = null;
    }

    public void add(int data){
        Node node = new Node(data);
        if (head == null) {
            this.head = node;
            this.tail = node;
            return;
        }
        tail.setNext(node);
        tail = node;
    }

    boolean empty() {
        if(head == null){
            return true;
        }
        return false;
    }
    public int peek() {
        return head.getData();
    }

    public int poll() {
        Node temp = head;
        head = head.getNext();
        return temp.getData();
    }

    public int size() {
        Node current = head;
        int size=0;
        if (head == null) {
            return 0;
        } else {
            while (current != null) {
                current = current.getNext();
                size++;
            }
        }
        return size;
    }

    public void show() {
            while (head.getNext()!= null) {
                System.out.println(head.getData());
                head = head.getNext();
            }
            System.out.println(head.getData());
        }
    }

