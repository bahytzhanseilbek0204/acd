package week5;

public class Node {
    private int data;
    private Node next;

    public Node(int data) {
        this.data = data;
        this.next = null;
    }
    public void setNext(Node nextData) {
        this.next = nextData;
    }
    public void setData(int data) {
        this.data = data;
    }
    public Node getNext() {
        return next;
    }

    public int getData(){
        return data;
    }


}



