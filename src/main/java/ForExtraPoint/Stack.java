package ForExtraPoint;

public class Stack {
    Node head;

    public void push(int data) {
        Node newNode = new Node(data);
        if (head == null) {
            head = newNode;
        } else {
            newNode.next = head;
            head = newNode;
        }
    }
     public int pop(){
        Node pop;
        pop = head;
        head = head.next;
        return pop.data;
    }
    public boolean empty() {
        if (head == null) {
            return true;
        } else {
            return false;
        }
    }
    public int size() {
        Node current = head;
        int size=0;
        if (head == null) {
            return 0;
        } else {
            while (current != null) {
                current = current.next;
                size++;
            }
        }
        return size;
    }
    public int top() {
        if (head == null) {
            return 0;
        } else {
            return head.data;
        }
    }
    public void show() {
        Node curr = head;

        while (curr.next!= null) {
            System.out.println(curr.data);
            curr = curr.next;
        }
        System.out.println(curr.data);
    }
}


