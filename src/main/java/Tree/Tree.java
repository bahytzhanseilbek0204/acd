package Tree;

public class Tree {
    Node root;

    Tree() {
        root = null;
    }

    public void insert( Integer key, String value) {
        Node node = new Node(key, value);
        if (root == null) {
            root = node;
        } else {
            Node current = root;
            while (current != null) {
                if (key < current.getKey()) {
                    if (current.getLeft() == null) {
                        current.setLeft(node);
                        return;
                    }
                    current = current.getLeft();
                } else if (key > current.getKey()) {
                    if (current.getRight() == null) {
                        current.setRight(node);
                        return;
                    }
                    current = current.getRight();

                }
            }

        }
    }



    public boolean delete() {
        return true;

    }
    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }

    public void printAll() {
        Node node = root;
        while (node != null) {
            if (node.getLeft() != null) {
                node = node.getLeft();
                return;

            }
            System.out.println(node.getValue());
            if (node.getRight() != null) {
                node = node.getRight();
            }
            return;
        }
        return;
    }
    public void ALLAscending(){
    PaAsc(this.root);
     }
    private void PaAsc(Node root) {
        if (root == null) {
            return;
        }
        PaAsc(root.getLeft());
        System.out.print(root.getValue() + " ");
        PaAsc(root.getRight());
    }
    public void delete(Integer key) {
        remove( this.root, key);
    }

    private void remove(Node current, int number) {
        Node temp;
        if(current==null)
            return;
        remove(current.getLeft(), number);
        if(current.getLeft()!=null)
            if(current.getLeft().getKey()==number){

            }
        if(current.getRight()!=null)
            if(current.getRight().getKey()==number){
               temp = current.getLeft();
            }
        remove(current.getRight(), number); // recursion checking the right side.

    }

   /* private Tree deleteNode(Tree root, Integer key) {

        if(root == null) return root;

        if(key < root.getKey()) {
            root.setLeft(deleteNode(root.getLeft(), data));
        } else if(data > root.getData()) {
            root.setRight(deleteNode(root.getRight(), data));
        } else {
            // node with no leaf nodes
            if(root.getLeft() == null && root.getRight() == null) {
                System.out.println("deleting "+data);
                return null;
            } else if(root.getLeft() == null) {
                // node with one node (no left node)
                System.out.println("deleting "+data);
                return root.getRight();
            } else if(root.getRight() == null) {
                // node with one node (no right node)
                System.out.println("deleting "+data);
                return root.getLeft();
            } else {
                // nodes with two nodes
                // search for min number in right sub tree
                Integer minValue = minValue(root.getRight());
                root.setData(minValue);
                root.setRight(deleteNode(root.getRight(), minValue));
                System.out.println("deleting "+data);
            }
        }

        return root;
    }*/
}


