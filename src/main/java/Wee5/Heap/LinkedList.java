package Wee5.Heap;

public class LinkedList {
        private Node head;
        private Node tail;

        public Node getHead() {
            return head;
        }

        public Node getTail() {
            return tail;
        }

        public void add(int data){
            Node node = new Node(data);
            if (head==null){
                head= node;
                tail= node;
            } else {
                tail.setNext(node);
                tail =node;
            }
        }

        public void Inserttostart(int value){
            Node newblock = new Node(value);
            newblock.setNext(head);
            head = newblock;
        }


        public void DeleteValue(int value){
            Node currentB = head, prev = null;
            if (value == currentB.getValue()){
                head = currentB.getNext();
                System.out.println("value deleted");
            } else {
                while (currentB!=null && currentB.getValue()!=value){
                    prev = currentB;
                    currentB = currentB.getNext();
                }
                if (currentB==null){
                    System.out.println("cant find value");
                } else {
                    prev.setNext(currentB.getNext());
                    System.out.println("value deleted");
                }
            }
        }

        public void DeleteValueofIndex(int index){
            Node currentB = head, prev = null;
            if (index == 0){
                head = currentB.getNext();
                System.out.println("value in index "+index+" deleted");
            } else{
                int counter = 0;
                while (currentB!=null){
                    if (counter==index){
                        prev.setNext(currentB.getNext());
                        System.out.println("value in index "+index+" deleted");
                        break;
                    }
                    prev = currentB;
                    currentB = currentB.getNext();
                    counter++;
                }
                if (currentB==null){
                    System.out.println("index is not declared");
                }
            }
        }

        public int get(int index){
            Node currentB = head;
            int counter = 0;
            while (currentB!=null){
                if (counter == index){
                    return currentB.getValue();
                }
                currentB = currentB.getNext();
                counter++;
            }
            return 0;
        }
    public void show() {
            Node curr = head;

        while (curr.getNext()!= null) {
            System.out.println(curr.getValue());
            curr = curr.getNext();
        }
        System.out.println(curr.getValue());
    }
}




