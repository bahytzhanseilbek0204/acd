package Quiz2;

public class LinkedList{ //nothing to change
    public Node head;
    private Node tail;

    public LinkedList(){
        this.head = new Node("head");
        tail = head;
    }

    public Node head(){
        return head;
    }

    public void add(Node node){
        tail.next = node;
        tail = node;
    }
    public void addFirst(String data){
        Node node = new Node(data);
        node.next =head;
        head =node;
        if(tail == null){
            tail = head;
        }
    }
}
