package Quiz2;

public class LinkedListTest {

    public static void main(String args[]) {
        //creating LinkedList with 5 elements including head
        LinkedList linkedList = new LinkedList();
        Node head = linkedList.head();
        linkedList.add( new Node("1"));
        linkedList.add( new Node("2"));
        linkedList.add( new Node("3"));
        linkedList.add( new Node("4"));
        linkedList.add( new Node("5"));
        linkedList.addFirst("9");

        Node middle = null;
        int length = 0;
        while( head != null){
            head = head.next;
            length++;
        }
        middle = linkedList.head;
        for(int i = 0; i < length/2 ; i++) {
            middle = middle.next;
        }
        /*while(current.next() != null){
                length++;
                if(length%2 ==0){
                    middle = middle.next();
                }
                current = current.next();
            }

            if(length%2 == 1){
                middle = middle.next();
            }*/
        System.out.println("length of LinkedList: " + length);
        System.out.println("middle element of LinkedList : " + middle);
    }
}

