package kz.aitu.week2;
import java.util.Scanner;
public class Problem22 {
        public static void main(String args[])
        {
            int reversenum =0;
            System.out.println("Input your number and press enter: ");
            Scanner in = new Scanner(System.in);
            int num = in.nextInt();
            while( num != 0 )
            {
                reversenum = reversenum * 10;
                reversenum = reversenum + num%10;
                num = num/10;
            }

            System.out.println("Reverse of input number is: "+reversenum);
        }
    }
