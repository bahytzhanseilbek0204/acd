package kz.aitu.week2;
import java.util.Scanner;

public class Problem3 {

        public void counter ( int count){
            if (count == 0) return;

            Scanner s = new Scanner(System.in);
            String b = s.nextLine();

            count = count - 1;

            counter(count);
            System.out.println(b);
        }
        public void run () {
            Scanner s = new Scanner(System.in);
            int a = s.nextInt();
            counter(a);
        }
    }

