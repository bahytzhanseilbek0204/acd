package kz.aitu.week2;
import java.util.Scanner;
public class Problem5 {
        static void r(int arr[], int n, int b, int index) {
            int i;
            if (b == 0) {
                for (i = 0; i < index; i++)
                    System.out.print(arr[i] + " ");
                System.out.println();
            }
            if (b > 0) {
                for (i = 1; i <= n; ++i) {
                    arr[index] = i;
                    r(arr, n, b - 1, index + 1);
                }
            }
        }
        public void run()
        {
            Scanner scan = new Scanner(System.in);
            int n = scan.nextInt();
            int k = scan.nextInt();
            int arr[] = new int[k];
            r(arr, n, k, 0);
        }
    }

