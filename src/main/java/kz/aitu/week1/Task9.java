package kz.aitu.week1;
import java.util.Scanner;

public class Task9 {
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            int n = in.nextInt();
            int k = in.nextInt();

            int result = binomial(n , k);

            System.out.println(result);

        }
        public static int binomial(int n, int k)
        {

            // Base Cases
            if (k == 0 || k == n)
                return 1;

            // Recur
            return binomial(n - 1, k - 1) + binomial(n - 1, k);
        }
    }

