package kz.aitu.week1;
import java.util.Scanner;

public class Task8 {
     public static void main(String[] args) {
            Scanner in = new Scanner(System.in);

            int a = in.nextInt();
            int b = in.nextInt();

            int gcd = 1;
            int k = 2;

            while (k <= a && k <= b) {
                if (a % k == 0 && b % k == 0) {
                    gcd = k;
                }
                k++;
            }

            System.out.println(gcd);
        }
    }
