package kz.aitu.week1;
import java.util.Scanner;
public class Task1 {
        public static String recursion(int n) {

            if (n == 1) {
                return "1";
            }
            return recursion(n - 1) + " " + n;
        }
        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            int n = in.nextInt();
            String s = recursion(n);
            System.out.println(s);
        }
    }




