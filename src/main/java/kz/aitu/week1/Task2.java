package kz.aitu.week1;

import java.util.Scanner;
public class Task2 {
    public static int findMax(int n) {
        if(n == 0) return 0;
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        return Math.max(n, findMax(a));
    }

    public static void main(String[] args) {
        System.out.println(findMax(1));
    }
}

