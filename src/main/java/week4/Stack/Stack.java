package week4.Stack;

public class Stack {
    Node head;

    public int pop(){
        Node pop;
        pop = head;
        head = head.next;
        return pop.data;
    }

    public void push(int data) {
        Node newNode = new Node(data);
        if (head == null) {
            head = newNode;
        } else {
            newNode.next = head;
            head = newNode;
        }
    }

    public boolean empty() {
        if (head == null) {
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        Node current = head;
        int size=0;
        if (head == null) {
            return 0;
        } else {
            while (current != null) {
                current = current.next;
                size++;
            }
        }
        return size;
    }

    public int top() {
        if (head == null) {
            return 0;
        } else {
            return head.data;
        }
    }
}


