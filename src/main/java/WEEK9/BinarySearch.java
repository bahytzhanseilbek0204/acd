package WEEK9;

import java.util.Scanner;

public class BinarySearch {
        public static void main(String[] args) {
            int[] arr = {16, 19, 20, 23, 45, 56, 78, 90, 96, 100, 125};
            int key, location;
            Scanner sc = new Scanner(System.in);
            key = sc.nextInt();
            location = binarySearch(arr,1,11,key);
            if(location != 0)
                System.out.println("location of key "+location);
            else
                System.out.println("Key not found");
        }
        public static int binarySearch(int[] arr, int beg, int end, int key)
        {
            int mid;
            if(end >= beg)
            {
                mid = (beg + end)/2;
                if(arr[mid] == key)
                {
                    return mid+1;
                }
                else if(arr[mid] < key)
                {
                    return binarySearch(arr,mid+1,end,key);
                }
                else
                {
                    return binarySearch(arr,beg,mid-1,key);
                }

            }
            return 0;
        }
    }

