package WEEK9;

import java.util.Scanner;

public class Sum4 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int[] arr = {1 , 2 ,3 ,4 ,5 ,6 ,7 ,8 ,9, 12};
        int length = arr.length;

        System.out.println("Enter your number: ");
        int n = in.nextInt();

        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                for (int k = j + 1; k < length; k++) {
                    for (int l = k + 1; l < length; l++) {
                        if (arr[i] + arr[j] + arr[k] + arr[l] == n) {
                            System.out.println(arr[i] + " + " + arr[j] + " + " + arr[k] + " + " + arr[l] + " = " + n);
                        }
                    }
                }
            }
        }

    }
}

