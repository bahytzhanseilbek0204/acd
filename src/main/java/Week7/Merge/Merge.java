package Week7.Merge;

public class Merge {
    /*public static void mergesort(int[] array) {
        mergesort(array, new int[array.length], 0, array.length - 1);
    }

    private static void mergesort(int[] array, int[] temp, int leftstart, int rightend) {
        if (leftstart >= rightend) {
            return;
        }
        int middle = (leftstart + rightend) / 2;
        mergesort(array, temp, leftstart, middle);
        mergesort(array, temp, middle + 1, rightend);
        mergeHalves(array, temp, leftstart, rightend);
    }

    public static void mergeHalves(int[] array, int[] temp, int leftstart, int rightend) {
        int leftend = (rightend + leftstart) / 2;
        int rightstart = leftend + 1;
        int size = rightend - leftstart + 1;

        int left = leftstart;
        int right = rightstart;
        int index = leftstart;

        while (left <= leftend && right <= rightend) {
            if (array[left] <= array[right]) {
                temp[index] = array[left];
                left++;
            } else {
                temp[index] = array[right];
                right++;
            }
            index++;
        }
        System.arraycopy(array, left, temp, index, leftend - left + 1);
        System.arraycopy(array, right, temp, index, rightend - right + 1);
        System.arraycopy(temp, leftstart, array, leftstart, size);
    }
    public void printall(int [] arr){
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

   */
     public void merge(int arr[], int beg, int mid, int end) {

         int l = mid - beg + 1;
         int r = end - mid;

        int LeftArray[] = new int [l];
        int RightArray[] = new int [r];

        for (int i=0; i<l; i++) {
            LeftArray[i] = arr[beg + i];
        }
        for (int j=0; j<r; j++) {
            RightArray[j] = arr[mid + 1 + j];
        }
        int i = 0, j = 0;
        int k = beg;
        while (i<l&&j<r) {
            if (LeftArray[i] <= RightArray[j]) {
                arr[k] = LeftArray[i];
                i++;
            }
            else {
                arr[k] = RightArray[j];
                j++;
            }
            k++;
        }

    }

    void sort(int arr[], int beg, int end)
    {
        if (beg<end)
        {
            int mid = (beg+end)/2;
            sort(arr, beg, mid);
            sort(arr , mid+1, end);
            merge(arr, beg, mid, end);
        }
    }
    public static void main(String args[])
    {
        int arr[] = {10,23,101,5,165};
        Merge ob = new Merge();
        ob.sort(arr, 0, arr.length-1);

        System.out.println("Sored:");
        for(int i =0; i<arr.length;i++) {
            System.out.println(arr[i]+"");
        }
    }
}


