package Week8.quiz;

public class List {

    private Student[] studentList;
    private int size = 0;

    public List() {
        studentList = new Student[150];
    }

    public void add(Student student) {
        studentList[size++] = student;
        size++;
    }

    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.println(studentList[i]);
        }
    }

    public void sort(String type, String order) {
        if(type.equals("name_a") && order.equals("asc")) {
            sort_name_a_asc();
        } else if(type.equals("name_a") && order.equals("desc")) {
            sort_name_a_desc();
        } else if(type.equals("name_l") && order.equals("asc")) {
            sort_name_l_asc();
        } else if(type.equals("name_l") && order.equals("desc")) {
            sort_name_l_desc();
        } else if(type.equals("age") && order.equals("asc")) {
            sort_age_asc();
        } else if(type.equals("age") && order.equals("desc")) {
            sort_age_desc();
        } else if(type.equals("gpa") && order.equals("asc")) {
            sort_gpa_asc();
        } else if(type.equals("gpa") && order.equals("desc")) {
            sort_gpa_desc();
        }
    }

    public void sort_name_a_asc() {

    }

    public void sort_name_a_desc() {

    }

    public void sort_name_l_asc() {

    }




    public void sort_name_l_desc() {

    }

    public void sort_age_asc() {
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getAge() < studentList[index].getAge()){
                    index = j;
                }
            }
            Student temp = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =temp;
        }

    }


    public void sort_age_desc() {
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getAge() > studentList[index].getAge()){
                    index = j;
                }
            }
            Student temp = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =temp;
        }


    }

    public void sort_gpa_asc() {
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getGpa() < studentList[index].getGpa()){
                    index = j;
                }
            }
            Student temp = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =temp;
        }

    }


    public void sort_gpa_desc() {
        for (int i = 0; i < size - 1; i++) {
            int index = i;
            for (int j = i + 1; j < size; j++){
                if (studentList[j].getGpa() > studentList[index].getGpa()){
                    index = j;
                }
            }
            Student temp = studentList[index];
            studentList[index] =(studentList[i]);
            studentList[i] =temp;
        }

    }


}

